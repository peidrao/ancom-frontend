# Amcom-Frontend


## Tecnologias usadas

- React
- Typescript
- Material UI
- Axios

## Start no projeto

É importate fazer o start na API primeiro, para que o frontend funcione corretamente.

1. **Instale as dependência do projeto (NPM)**

```bash
npm i 
```

ou 

```bash
npm install --force
```

2. **Rode o projeto**

```bash
npm run start
```

## Sobre o projeto

A única etapa que não foi concluída foi a atualização da venda. Acabei deixando-a como uma das últimas tarefas a serem realizadas e encontrei dificuldades para implementar as regras na tela.

Apesar de não ter conseguido concluir todas as tarefas, as outras foram executadas de forma a ficar o mais próximo possível do sugerido pelo Figma. Alguns detalhes, como o alerta e o espaçamento, e outros pequenos elementos, não foram reproduzidos com total fidelidade.

Foram realizadas algumas validações, como, por exemplo, no filtro, onde o botão só é habilitado quando pelo menos uma das datas é inserida e só funciona quando ambos os campos de data estão preenchidos.

## O que teria feito diferente

- Usar o Vite.
- Globalizar melhor as estilizações.
- Componentizar melhor as telas (principalmente as que envolvem tabelas).
- Formatar melhor os valores decimais.
- Melhorar a organização de pastas.
- Adicionar algum tipo de spinner para o carregamento das páginas.
