import { createAsyncThunk } from '@reduxjs/toolkit';
import { SaleRequestPayload } from '../interfaces/Sale';
import axiosInstance from '../config/axios.config';

export const fetchSales = createAsyncThunk('sales/fetchSales', async () => {
  const response = await axiosInstance.get('/sales/');
  const data = await response.data;
  return data;
});

export const createSale = createAsyncThunk(
  'sales/createSale',
  async (payload: SaleRequestPayload) => {
    const response = await axiosInstance.post('/sales/', payload);

    const data = await response.data;
    return data;
  },
);

export const removeSale = createAsyncThunk(
  'sales/removeSale',
  async (saleId: number) => {
    const response = await axiosInstance.delete(`/sales/${saleId}`);
    const data = await response.data;
    return data;
  },
);

export const retrieveSale = createAsyncThunk(
  'sales/retrieveSale',
  async (saleId: number) => {
    const response = await axiosInstance.get(`/sales/${saleId}`);
    const data = await response.data;
    return data;
  },
);
