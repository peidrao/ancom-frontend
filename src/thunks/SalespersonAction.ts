import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosInstance from '../config/axios.config';

export const fetchSalesperson = createAsyncThunk(
  'salesperson/fetchSalesperson',
  async () => {
    const response = await axiosInstance.get('/salespersons/');
    const data = await response.data;
    return data;
  },
);
