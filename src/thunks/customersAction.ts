import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosInstance from '../config/axios.config';

export const fetchCustomers = createAsyncThunk(
  'customers/fetchCustomers',
  async () => {
    const response = await axiosInstance.get('/customers/');
    const data = await response.data;
    return data;
  },
);
