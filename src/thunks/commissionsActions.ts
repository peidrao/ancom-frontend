import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosInstance from '../config/axios.config';

interface DateProps {
  initial_period: string;
  final_period: string;
}

export const fetchCommissions = createAsyncThunk(
  'commissions/fetchCommissions',
  async (params: DateProps) => {
    const { initial_period, final_period } = params;

    const query = `initial_period=${initial_period}&final_period=${final_period}`;

    const response = await axiosInstance.get(`/commissions/?${query}`);
    const data = await response.data;
    return data;
  },
);
