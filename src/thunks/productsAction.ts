import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosInstance from '../config/axios.config';

export const fetchProducts = createAsyncThunk(
  'products/fetchProducts',
  async () => {
    const response = await axiosInstance.get('/products/');
    const data = await response.data;
    return data;
  },
);
