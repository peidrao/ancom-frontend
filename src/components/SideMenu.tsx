import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import { Link } from 'react-router-dom';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import React from 'react';
import sideMenuStyle from './SideMenu.style';

interface SideMenuProps {
  drawerOpen: boolean;
  setDrawerOpen: (value: boolean) => void;
}

const SideMenu = ({ drawerOpen, setDrawerOpen }: SideMenuProps) => {
  const { classes } = sideMenuStyle();

  return (
    <Drawer
      anchor="left"
      open={drawerOpen}
      onClose={() => setDrawerOpen(false)}
      classes={{ paper: classes.paper }}
    >
      <List className={classes.drawer}>
        <ListItem
          onClick={() => setDrawerOpen(false)}
          className={classes.listItem}
        >
          <ListItemIcon className={classes.listItemIcon}>
            <AttachMoneyIcon />
          </ListItemIcon>
          <Link to="/" className={classes.listItemText}>
            <ListItemText primary="Vendas" />
          </Link>
        </ListItem>
        <ListItem
          onClick={() => setDrawerOpen(false)}
          className={classes.listItem}
        >
          <ListItemIcon className={classes.listItemIcon}>
            <MonetizationOnIcon />
          </ListItemIcon>
          <Link to="/commissions" className={classes.listItemText}>
            <ListItemText primary="Comissões" className={classes.itemText} />
          </Link>
        </ListItem>
      </List>
    </Drawer>
  );
};

export default SideMenu;
