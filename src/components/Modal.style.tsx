import { makeStyles } from 'tss-react/mui';

const modalStyle = makeStyles()({
  dialogTitle: {
    color: '#42474A',
  },
  noButton: {
    color: '#00585E',
    border: '1px solid #00585E',
  },
  yesButton: {
    color: 'white',
    backgroundColor: '#00585E',
  },
});

export default modalStyle;
