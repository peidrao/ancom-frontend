import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Divider,
} from '@mui/material';
import modalStyle from './Modal.style';

interface SaleModalProps {
  title: string;
  text: string;
  onClose: () => void;
  isOpen: boolean;
  handleAction?: () => void;
}

const SaleModal = ({
  title,
  text,
  handleAction,
  onClose,
  isOpen,
}: SaleModalProps) => {
  const { classes } = modalStyle();

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      hideBackdrop={true}
      PaperProps={{
        elevation: 0,
        sx: { border: 'solid 1px #00585E', width: '30%', maxHeight: '30%' },
      }}
    >
      <DialogTitle className={classes.dialogTitle}>{title}</DialogTitle>
      <Divider />
      <DialogContent>
        <DialogContentText>{text}</DialogContentText>
      </DialogContent>
      <Divider />
      <DialogActions>
        <Button className={classes.noButton} onClick={onClose}>
          Não
        </Button>
        <Button className={classes.yesButton} onClick={handleAction}>
          Sim
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SaleModal;
