import { makeStyles } from 'tss-react/mui';

const navbarStyle = makeStyles()({
  container: {
    flexGrow: 1,
    textAlign: 'center',
    color: '#2B7D83',
    fontSize: '24px',
    fontWeight: 'bold',
  },
});

export default navbarStyle;
