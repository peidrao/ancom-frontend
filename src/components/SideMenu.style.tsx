import { makeStyles } from 'tss-react/mui';

const sideMenuStyle = makeStyles()({
  drawer: {
    width: '250px',
  },
  listItem: {
    padding: 20,
    marginTop: 10,
    height: 50,
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  listItemIcon: {
    marginRight: 10,
    color: '#2B7D83',
  },
  listItemText: {
    textDecoration: 'none',
    color: '#2B7D83',
  },
  itemText: {
    fontWeight: 'bold',
  },
  paper: {
    backgroundColor: '#F2F2F2',
  },
});

export default sideMenuStyle;
