import { Alert as AlerMaterialUI, AlertColor, Snackbar } from '@mui/material';

interface AlertProps {
  alertOpen: boolean;
  handleAlertClose: () => void;
  text: string;
  severity: string;
}

const Alert = ({ alertOpen, handleAlertClose, text, severity }: AlertProps) => {
  return (
    <Snackbar
      open={alertOpen}
      onClose={handleAlertClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
    >
      <AlerMaterialUI
        onClose={handleAlertClose}
        severity={severity as AlertColor}
      >
        {text}
      </AlerMaterialUI>
    </Snackbar>
  );
};

export default Alert;
