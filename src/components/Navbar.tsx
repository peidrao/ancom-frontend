import React, { useState } from 'react';
import { AppBar, Toolbar, IconButton, Box } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

import Logo from '../assets/image_1-removebg-preview.png';
import SideMenu from './SideMenu';
import navbarStyle from './Navbar.style';

interface NavBarProps {
  title: string;
}

const Navbar = ({ title }: NavBarProps) => {
  const [drawerOpen, setDrawerOpen] = useState(false);

  const { classes } = navbarStyle();

  return (
    <>
      <AppBar position="static" sx={{ backgroundColor: '#F0F0F0' }}>
        <Toolbar>
          <IconButton onClick={() => setDrawerOpen(true)}>
            <MenuIcon sx={{ color: '#2B7D83' }} />
          </IconButton>
          <img src={Logo} alt="Logo" style={{ marginLeft: '20px' }} />
          <Box className={classes.container}>{title}</Box>
        </Toolbar>
      </AppBar>
      <SideMenu drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} />
    </>
  );
};

export default Navbar;
