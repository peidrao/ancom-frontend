import React, { useEffect, useState } from 'react';
import {
  Typography,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Collapse,
  Box,
} from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import EditIcon from '@mui/icons-material/Edit';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { fetchSales, removeSale } from '../thunks/salesAction';
import { formatDate } from '../utils/formatDate';
import SaleModal from '../components/Modal';
import { Sale } from '../interfaces/Sale';
import { ProductSet } from '../interfaces/Product';
import Navbar from '../components/Navbar';
import Alert from '../components/Alert';
import salestyle from './styles/SalesPage.style';

const SalesPage: React.FC = () => {
  const { classes } = salestyle();
  const location = useLocation();
  const [openRow, setOpenRow] = useState<number | null>(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [saleItemRemove, setSaleItemRemove] = useState(0);
  const [alertMessage, setAlertMessage] = useState('');
  const navigate = useNavigate();
  const sales = useSelector((state: any) => state.sales.sales);

  const handleAddSale = () => {
    navigate('/new-sale');
  };

  const handleOpenModal = (saleId: number) => {
    setIsModalOpen(true);
    setSaleItemRemove(saleId);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };
  const dispatch = useDispatch<any>();

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const message = searchParams.get('alertMessage');
    if (message) {
      setAlertMessage(message);
    }
    dispatch(fetchSales()).unwrap();
  }, [dispatch, location.search]);

  const handleToRemoveSale = () => {
    dispatch(removeSale(saleItemRemove));
    handleCloseModal();
  };

  return (
    <>
      <Navbar title="Vendas" />
      <Box
        style={{
          display: 'flex',
          flexDirection: 'column',
          paddingTop: '20px',
          margin: '20px',
        }}
      >
        <Box
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: '20px',
            marginTop: '20px',
          }}
        >
          <Typography
            variant="h4"
            style={{ color: '#00585E', fontWeight: 'bold' }}
          >
            Vendas Realizadas
          </Typography>

          <Alert
            alertOpen={Boolean(alertMessage)}
            handleAlertClose={() => setAlertMessage('')}
            text={alertMessage}
            severity="success"
          />

          <Button
            onClick={handleAddSale}
            variant="contained"
            style={{ background: '#00585E', textTransform: 'lowercase' }}
          >
            Inserir Nova Venda
          </Button>
        </Box>

        <TableContainer
          style={{ maxHeight: '600px', overflowY: 'auto', marginTop: '20px' }}
        >
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Nota Fiscal</TableCell>
                <TableCell>Cliente</TableCell>
                <TableCell>Vendedor</TableCell>
                <TableCell>Data de Venda</TableCell>
                <TableCell>Valor Total</TableCell>
                <TableCell>Opções</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {sales.map((sale: Sale, index: number) => (
                <React.Fragment key={`row-${sale.invoice_number}`}>
                  <TableRow key={sale.id}>
                    <TableCell>{sale.invoice_number}</TableCell>
                    <TableCell>{sale.customer_name}</TableCell>
                    <TableCell>{sale.sales_person_name}</TableCell>
                    <TableCell>{formatDate(sale.created_at)}</TableCell>
                    <TableCell>R$ {sale.total_value.toFixed(2)}</TableCell>
                    <TableCell>
                      <Button
                        variant="text"
                        style={{ color: '#00585E' }}
                        onClick={() => {
                          if (openRow === index) {
                            setOpenRow(null);
                          } else {
                            setOpenRow(index);
                          }
                        }}
                      >
                        Ver Items
                      </Button>
                      <Button variant="text">
                        <Link to={`update-sale/${sale.id}`}>
                          <EditIcon style={{ color: '#00585E' }} />
                        </Link>
                      </Button>
                      <Button
                        variant="text"
                        onClick={() => handleOpenModal(sale.id)}
                      >
                        <DeleteOutlineIcon style={{ color: '#BE0000' }} />
                      </Button>
                      <SaleModal
                        isOpen={isModalOpen}
                        onClose={handleCloseModal}
                        title="Remover Venda"
                        text="Deseja remover esta venda?"
                        handleAction={() => {
                          handleToRemoveSale();
                        }}
                      />
                    </TableCell>
                  </TableRow>
                  <TableRow key={sale.invoice_number}>
                    <TableCell
                      style={{ paddingBottom: 0, paddingTop: 0 }}
                      colSpan={12}
                    >
                      <Collapse
                        in={index === openRow}
                        timeout="auto"
                        unmountOnExit
                      >
                        <Box sx={{ margin: 1 }}>
                          <Table size="small">
                            <TableHead>
                              <TableRow>
                                <TableCell style={{ border: 'none' }}>
                                  Produtos/Serviço
                                </TableCell>
                                <TableCell style={{ border: 'none' }}>
                                  Quantidade
                                </TableCell>
                                <TableCell style={{ border: 'none' }}>
                                  Preço unitário
                                </TableCell>
                                <TableCell style={{ border: 'none' }}>
                                  Total do Produto
                                </TableCell>
                                <TableCell style={{ border: 'none' }}>
                                  % de Comissão
                                </TableCell>
                                <TableCell style={{ border: 'none' }}>
                                  Comissão
                                </TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {sale.products_set.map((product: ProductSet) => (
                                <TableRow key={product.id}>
                                  <TableCell style={{ border: 'none' }}>
                                    {product.name}
                                  </TableCell>
                                  <TableCell style={{ border: 'none' }}>
                                    {product.quantity}
                                  </TableCell>
                                  <TableCell style={{ border: 'none' }}>
                                    R$ {product.value}
                                  </TableCell>
                                  <TableCell style={{ border: 'none' }}>
                                    R$ {product.total_value.toFixed(2)}
                                  </TableCell>
                                  <TableCell style={{ border: 'none' }}>
                                    {product.commission_percentage} %
                                  </TableCell>
                                  <TableCell style={{ border: 'none' }}>
                                    R$ {product.commission.toFixed(2)}
                                  </TableCell>
                                </TableRow>
                              ))}
                              <TableRow>
                                <TableCell className={classes.footerTable}>
                                  Total da Venda
                                </TableCell>
                                <TableCell className={classes.footerTable}>
                                  {sale.products_set.reduce(
                                    (acc: any, product: ProductSet) =>
                                      acc + product.quantity,
                                    0,
                                  )}
                                </TableCell>
                                <TableCell
                                  className={classes.footerTable}
                                ></TableCell>
                                <TableCell className={classes.footerTable}>
                                  R${' '}
                                  {sale.products_set
                                    .reduce(
                                      (acc: any, product: ProductSet) =>
                                        acc + product.total_value,
                                      0,
                                    )
                                    .toFixed(2)}
                                </TableCell>
                                <TableCell
                                  className={classes.footerTable}
                                ></TableCell>
                                <TableCell className={classes.footerTable}>
                                  R${' '}
                                  {sale.products_set
                                    .reduce(
                                      (acc: any, product: ProductSet) =>
                                        acc + product.commission,
                                      0,
                                    )
                                    .toFixed(2)}
                                </TableCell>
                              </TableRow>
                            </TableBody>
                          </Table>
                        </Box>
                      </Collapse>
                    </TableCell>
                  </TableRow>
                </React.Fragment>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </>
  );
};

export default SalesPage;
