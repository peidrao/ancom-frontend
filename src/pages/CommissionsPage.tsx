import React, { useEffect, useState } from 'react';
import {
  Box,
  Button,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import { Dayjs } from 'dayjs';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { Search } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCommissions } from '../thunks/commissionsActions';
import commissionsStyle from './styles/CommissionsPage.style';
import Navbar from '../components/Navbar';
import Alert from '../components/Alert';

interface CommissionDetail {
  total: number;
  count: number;
}

interface Commission {
  name: number;
  id: number;
  commission: CommissionDetail;
}

const CommissionsPage = () => {
  const dispatch = useDispatch<any>();
  const [openAlert, setOpenAlert] = useState<boolean>(false);
  const [initialPeriod, setInititalPeriod] = useState<Dayjs | null>(null);
  const [finalPeriod, setFinalPeriod] = useState<Dayjs | null>(null);
  const [isCommissionsEmpty, setIsCommissionsEmpty] = useState(true);
  const commissions = useSelector(
    (state: any) => state.commissions.commissions,
  );

  const { classes } = commissionsStyle();

  const handleToSearchCommissions = () => {
    if (initialPeriod && finalPeriod) {
      if (initialPeriod > finalPeriod) {
        setOpenAlert(true);
        return;
      } else {
        dispatch(
          fetchCommissions({
            initial_period: initialPeriod.format('YYYY-MM-DD'),
            final_period: finalPeriod.format('YYYY-MM-DD'),
          }),
        ).unwrap();
      }
    }
  };

  useEffect(() => {
    setIsCommissionsEmpty(commissions.length === 0);
  });

  return (
    <>
      <Navbar title="Comissões" />
      <Box className={classes.container}>
        <Box className={classes.header}>
          <Typography variant="h4" className={classes.headerText}>
            Relatório de Comissões
          </Typography>

          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DemoContainer components={['DatePicker', 'DatePicker']}>
              <DatePicker
                label={
                  <span style={{ fontWeight: 'bold', color: '#A4A4A4' }}>
                    Período de Início
                  </span>
                }
                value={initialPeriod}
                onChange={setInititalPeriod}
                sx={{
                  '& .MuiIconButton-root': {
                    color: '#00585E',
                  },
                }}
              />
              <DatePicker
                label={
                  <span style={{ fontWeight: 'bold', color: '#A4A4A4' }}>
                    Período de Fim
                  </span>
                }
                value={finalPeriod}
                onChange={setFinalPeriod}
                sx={{
                  '& .MuiIconButton-root': {
                    color: '#00585E',
                  },
                }}
              />
              <Button
                onClick={handleToSearchCommissions}
                className={classes.headerButtonSearch}
                disabled={initialPeriod === null && finalPeriod === null}
              >
                <Search className={classes.headerButtonIcon} />
              </Button>
              <Alert
                alertOpen={openAlert}
                handleAlertClose={() => setOpenAlert(false)}
                text="Período de Início não deve ser maior que o Período de Fim"
                severity="error"
              />
            </DemoContainer>
          </LocalizationProvider>
        </Box>

        <Box>
          {isCommissionsEmpty && (
            <Stack
              sx={{
                height: '60vh',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography
                variant="h5"
                style={{
                  fontSize: '16px',
                }}
              >
                Para visualizar o relatório, selecione um período nos campos
                acima.
              </Typography>
            </Stack>
          )}

          {!isCommissionsEmpty && (
            <TableContainer
              style={{
                maxHeight: '600px',
                overflowY: 'auto',
                marginTop: '20px',
              }}
            >
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Cód.</TableCell>
                    <TableCell>Vendedor</TableCell>
                    <TableCell>Total de Vendas</TableCell>
                    <TableCell>Total de Comissões</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {commissions.map((commission: Commission) => (
                    <TableRow key={commission.id}>
                      <TableCell>{commission.id}</TableCell>
                      <TableCell>{commission.name}</TableCell>
                      <TableCell>{commission.commission.count}</TableCell>
                      <TableCell>
                        R$ {commission.commission.total.toFixed(2)}
                      </TableCell>
                    </TableRow>
                  ))}
                  <TableRow>
                    <TableCell className={classes.footerTable}>
                      Total de Comissões do Período:
                    </TableCell>
                    <TableCell />
                    <TableCell />
                    <TableCell className={classes.footerTable}>
                      R$
                      {commissions
                        .reduce(
                          (acc: any, commission: Commission) =>
                            acc + commission.commission.total,
                          0,
                        )
                        .toFixed(2)}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </Box>
      </Box>
    </>
  );
};

export default CommissionsPage;
