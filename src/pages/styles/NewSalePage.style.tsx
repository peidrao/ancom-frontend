import { makeStyles } from 'tss-react/mui';

const newSaletyle = makeStyles()({
  container: {
    display: 'flex',
  },
  containerProducts: {
    flex: 1,
    padding: '20px',
    borderRight: '1px solid #ccc',
  },
  containerFormSale: {
    flex: 1,
    padding: '20px',
  },
  formSale: {
    display: 'flex',
    flexDirection: 'column',
    gap: '30px',
  },
  tableHeadTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    border: 'none',
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  labelInput: {
    fontSize: 15,
    fontWeight: 400,
    color: '#000000',
  },
  button: {
    background: '#00585E',
    color: 'white',
    textTransform: 'capitalize',
    ':hover': {
      background: '#004145',
    },
  },
});

export default newSaletyle;
