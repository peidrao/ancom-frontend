import { makeStyles } from 'tss-react/mui';

const commissionsStyle = makeStyles()({
  container: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '20px',
    margin: '20px',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    color: '#00585E',
    fontWeight: 'bold',
  },
  headerButtonSearch: {
    justifyContent: 'center',
    alignItems: 'center',
    background: '#00585E',
    ':hover': {
      background: '#004145',
    },
  },
  headerButtonIcon: {
    color: 'white',
  },
  footerTable: {
    fontWeight: 'bold',
  },
});

export default commissionsStyle;
