import { makeStyles } from 'tss-react/mui';

const salestyle = makeStyles()({
  footerTable: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#00585E',
    border: 'none',
  },
});

export default salestyle;
