import { useDispatch, useSelector } from 'react-redux';
import Navbar from '../components/Navbar';
import { SyntheticEvent, useEffect, useState } from 'react';
import { createSale, retrieveSale } from '../thunks/salesAction';
import { Sale, SaleItem, SaleRequestPayload } from '../interfaces/Sale';
import {
  Autocomplete,
  Box,
  Button,
  FilterOptionsState,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';
import { Product } from '../interfaces/Product';
import newSaletyle from './styles/NewSalePage.style';
import { useNavigate } from 'react-router-dom';
import dayjs, { Dayjs } from 'dayjs';
import { fetchCustomers } from '../thunks/customersAction';
import { fetchSalesperson } from '../thunks/SalespersonAction';
import { fetchProducts } from '../thunks/productsAction';
import { Person } from '../interfaces/Person';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';

interface ProductItem {
  id: number;
  name: string;
  quantity: number;
  value: number;
  total_value: number;
}

const UpdateSalePage = () => {
  const dispatch = useDispatch<any>();
  const match = location.pathname.match(/\/update-sale\/(\d+)/);
  const saleId = match ? parseInt(match[1], 10) : null;

  const { classes } = newSaletyle();
  const [salesItem, setSalesItem] = useState<SaleItem[] | []>([]);
  const [product, setProduct] = useState<Product | null>();

  const [totalPrice, setTotalPrice] = useState<number>(0);
  const [createdAt, setCreatedAt] = useState<Dayjs | null>(dayjs());

  const [customer, setCustomer] = useState<number | null>(null);
  const [salesperson, setSalesperson] = useState<number | null>(null);

  const [quantity, setQuantity] = useState<number | null>();
  const navigate = useNavigate();

  const customers = useSelector((state: any) => state.customers.customers);
  const salespersons = useSelector(
    (state: any) => state.salespersons.salespersons,
  );

  const products_array = useSelector((state: any) => state.products.products);

  const sale: Sale = useSelector((state: any) => state.sales.sale);
  console.log(sale);
  if (sale) {
    console.log(createdAt);
    console.log(sale.created_at);
  }

  useEffect(() => {
    if (saleId) {
      dispatch(retrieveSale(saleId));
    }
    dispatch(fetchCustomers());
    dispatch(fetchSalesperson());
    dispatch(fetchProducts());
  }, [dispatch]);

  const isFormValid = (): boolean => {
    return (
      salesItem?.length >= 1 &&
      customer !== null &&
      salesperson !== null &&
      createdAt !== null
    );
  };

  const handleCreateToSave = () => {
    const data: SaleRequestPayload = {
      customer: Number(customer),
      sales_person: Number(salesperson),
      created_at: createdAt,
      products: salesItem,
    };
    dispatch(createSale(data))
      .then(() => {
        const message = 'Venda criada com sucesso!';
        navigate(`/?alertMessage=${encodeURIComponent(message)}`);
      })
      .catch((error: any) => {
        console.error('Erro ao criar venda:', error);
      });
  };

  const handleAddProduct = () => {
    if (product && quantity) {
      const newSaleItem: SaleItem = {
        product,
        quantity,
      };
      if (salesItem) {
        const totalPriceAmount = totalPrice + quantity * product.value;
        setTotalPrice(totalPriceAmount);
        setSalesItem([...salesItem, newSaleItem]);
      }
    }
    setQuantity(0);
  };

  const formatMoney = (value: number) => {
    return value.toFixed(2);
  };

  const filterOptions = (
    options: Product[],
    state: FilterOptionsState<Product>,
  ) => {
    return options.filter((option) => {
      const inputValue = state.inputValue;
      return (
        option.code.toLowerCase().includes(inputValue.toLowerCase()) ||
        option.description.toLowerCase().includes(inputValue.toLowerCase())
      );
    });
  };

  const handleToSales = () => {
    navigate('/');
  };

  const handleOnChange = (
    event: SyntheticEvent<Element, Event>,
    value: string | Product | null,
  ) => {
    if (value === null) {
      return;
    }
    setProduct(value as Product);
  };

  const handleRemoveSaleItem = (saleItem: ProductItem) => {
    console.log(saleItem);
    // if (salesItem) {
    //   setSalesItem(salesItem.filter((item) => item !== saleItem));
    //   const newTotalPrice =
    //     totalPrice - saleItem.quantity * saleItem.product.value;
    //   setTotalPrice(newTotalPrice);
    // }
  };

  return (
    <>
      <Navbar title={`Alterar Venda Nº ${sale?.id}`} />
      {sale && (
        <Grid container>
          <Grid padding={5} sm={8}>
            <Grid xs={12}>
              <Typography variant="h5">Produtos</Typography>
            </Grid>

            <Grid
              container
              style={{ gap: 15, marginTop: 22 }}
              justifyItems="center"
            >
              <Grid>
                <InputLabel>
                  Buscar pelo código de barras ou descrição
                </InputLabel>
                <Autocomplete
                  id="combo-box-demo"
                  freeSolo
                  options={products_array}
                  renderInput={(params) => <TextField {...params} />}
                  getOptionLabel={(option: string | Product) => {
                    if (typeof option === 'string') {
                      return option;
                    } else {
                      return option.description;
                    }
                  }}
                  filterOptions={filterOptions}
                  onChange={handleOnChange}
                />
              </Grid>

              <Grid>
                <InputLabel>Quantidade</InputLabel>
                <TextField
                  value={quantity}
                  onChange={(e) => setQuantity(Number(e.target.value))}
                  type="number"
                />
              </Grid>

              <Grid>
                <Button
                  className={classes.button}
                  size="large"
                  onClick={handleAddProduct}
                  style={{ marginTop: 26, height: 50 }}
                >
                  Adicionar
                </Button>
              </Grid>
            </Grid>

            <Grid container marginTop={5}>
              {sale.products_set && (
                <TableContainer>
                  <Table size="small" style={{ border: 'none' }}>
                    <TableHead>
                      <TableRow style={{ border: 'none' }}>
                        <TableCell style={{ border: 'none' }}>
                          Produto/Serviço
                        </TableCell>
                        <TableCell style={{ border: 'none' }}>
                          Quantidade
                        </TableCell>
                        <TableCell style={{ border: 'none' }}>
                          Preço Unitário
                        </TableCell>
                        <TableCell style={{ border: 'none' }}>Total</TableCell>
                        <TableCell style={{ border: 'none' }}></TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {sale.products_set.map((saleItem: ProductItem) => (
                        <TableRow key={saleItem.id} style={{ border: 'none' }}>
                          <TableCell style={{ border: 'none' }}>
                            {saleItem.name}
                          </TableCell>
                          <TableCell style={{ border: 'none' }}>
                            {saleItem.quantity}
                          </TableCell>
                          <TableCell style={{ border: 'none' }}>
                            {formatMoney(Number(saleItem.value))}
                          </TableCell>
                          <TableCell style={{ border: 'none' }}>
                            {formatMoney(saleItem.total_value)}
                          </TableCell>
                          <TableCell style={{ border: 'none' }}>
                            <Button
                              style={{ color: 'red' }}
                              onClick={() => handleRemoveSaleItem(saleItem)}
                            >
                              <DeleteOutlineIcon />
                            </Button>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              )}
            </Grid>
          </Grid>

          <Grid sm={4} padding={5}>
            <Typography variant="h5">Dados da Venda</Typography>

            <Box className={classes.formSale}>
              <Box style={{ flex: 1 }}>
                <InputLabel>Data e Hora da Venda</InputLabel>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={['DateTimePicker']}>
                    <DateTimePicker
                      value={
                        sale.created_at ? dayjs(sale.created_at) : createdAt
                      }
                      onChange={setCreatedAt}
                    />
                  </DemoContainer>
                </LocalizationProvider>
              </Box>
              <Box style={{ flex: 1 }}>
                <InputLabel>Escolha um vendedor</InputLabel>
                <Select
                  value={salesperson ? salesperson : sale.sales_person_name}
                  onChange={(e) => setSalesperson(e.target.value as number)}
                  style={{ width: '100%' }}
                >
                  <MenuItem value="">Selecione um vendedor</MenuItem>
                  {salespersons.map((salesperson: Person) => (
                    <MenuItem key={salesperson.id} value={salesperson.id}>
                      {salesperson.name}
                    </MenuItem>
                  ))}
                </Select>
              </Box>

              <Box style={{ flex: 1 }}>
                <InputLabel>Escolha um cliente</InputLabel>
                <Select
                  value={customer || null}
                  onChange={(e) => setCustomer(e.target.value as number)}
                  style={{ width: '100%' }}
                >
                  <MenuItem value="">Selecione um cliente</MenuItem>
                  {customers.map((customer: Person) => (
                    <MenuItem key={customer.id} value={customer.id}>
                      {customer.name}
                    </MenuItem>
                  ))}
                </Select>
              </Box>

              <Box
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <InputLabel
                  style={{
                    fontFamily: 'Roboto',
                    fontSize: '18px',
                    fontWeight: '600',
                    lineHeight: '24px',
                    letterSpacing: '0em',
                    textAlign: 'left',
                  }}
                >
                  Valor Total
                </InputLabel>
                <Typography
                  style={{
                    fontFamily: 'Roboto',
                    fontSize: '28px',
                    fontWeight: '700',
                    lineHeight: '37px',
                    letterSpacing: '0em',
                    textAlign: 'right',
                  }}
                >
                  R$ {totalPrice.toFixed(2)}
                </Typography>
              </Box>
            </Box>

            <Box className={classes.containerButton}>
              <Button
                className={classes.button}
                size="large"
                onClick={handleToSales}
              >
                Cancelar
              </Button>
              <Button
                className={classes.button}
                size="large"
                disabled={!isFormValid()}
                onClick={handleCreateToSave}
                style={{
                  backgroundColor: !isFormValid() ? '#BBD3D5' : '#00585E',
                  color: !isFormValid() ? 'white' : 'white',
                }}
              >
                Finalizar
              </Button>
            </Box>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default UpdateSalePage;
