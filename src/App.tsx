import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import SalesPage from './pages/SalesPage';
import NewSalePage from './pages/NewSalePage';
import CommissionsPage from './pages/CommissionsPage';
import UpdateSalePage from './pages/UpdateSalePage';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="" element={<SalesPage />} />
        <Route path="new-sale" element={<NewSalePage />} />
        <Route path="commissions" element={<CommissionsPage />} />
        <Route path="update-sale/:id" element={<UpdateSalePage />} />
      </Routes>
    </Router>
  );
}

export default App;
