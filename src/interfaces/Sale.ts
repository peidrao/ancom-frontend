import { Dayjs } from 'dayjs';
import { Product, ProductSet } from './Product';

export interface SaleItem {
  product: Product;
  quantity: number;
}

export interface Sale {
  id: number;
  customer_name: string;
  sales_person_name: string;
  created_at: string;
  invoice_number: string;
  total_value: number;
  products_set: ProductSet[];
}

export interface SaleRequestPayload {
  customer: number;
  sales_person: number;
  created_at: Dayjs | null;
  products: SaleItem[] | null;
}
