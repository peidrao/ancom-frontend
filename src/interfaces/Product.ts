export interface Product {
  id: number;
  code: string;
  description: string;
  value: number;
  commission_percentage: number;
}

export interface ProductSet {
  id: number;
  name: string;
  value: number;
  quantity: number;
  commission_percentage: number;
  commission: number;
  total_value: number;
}
