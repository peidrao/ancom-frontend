import { configureStore } from '@reduxjs/toolkit';
import salesReducer from './reducers/salesReducer';
import customersReducer from './reducers/customersReducer';
import salespersonReducer from './reducers/salespersonReducer';
import productsReducer from './reducers/productsReducer';
import commissionsReducer from './reducers/commissionsReducer';

const store = configureStore({
  reducer: {
    sales: salesReducer,
    customers: customersReducer,
    salespersons: salespersonReducer,
    products: productsReducer,
    commissions: commissionsReducer,
  },
});

export default store;
