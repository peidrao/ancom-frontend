import { createSlice } from '@reduxjs/toolkit';
import { fetchCommissions } from '../thunks/commissionsActions';

interface CommissionState {
  commissions: any[];
}

const initialState: CommissionState = {
  commissions: [],
};

const customerSlice = createSlice({
  name: 'commissions',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCommissions.fulfilled, (state, action) => {
      state.commissions = action.payload;
    });
  },
});

export default customerSlice.reducer;
