import { createSlice } from '@reduxjs/toolkit';
import {
  createSale,
  fetchSales,
  removeSale,
  retrieveSale,
} from '../thunks/salesAction';

interface SalesState {
  sales: any[];
  sale: any;
}

const initialState: SalesState = {
  sales: [],
  sale: null,
};

const salesSlice = createSlice({
  name: 'sales',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchSales.fulfilled, (state, action) => {
        state.sales = action.payload;
      })
      .addCase(createSale.fulfilled, (state, action) => {
        state.sales.push(action.payload);
      })
      .addCase(removeSale.fulfilled, (state, action) => {
        state.sales = state.sales.filter(
          (sale) => sale.id !== action.payload.id,
        );
      })
      .addCase(retrieveSale.fulfilled, (state, action) => {
        state.sale = action.payload;
      });
  },
});

export default salesSlice.reducer;
