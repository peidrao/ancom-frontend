import { createSlice } from '@reduxjs/toolkit';
import { fetchSalesperson } from '../thunks/SalespersonAction';

interface SalesPersonState {
  salespersons: any[];
}

const initialState: SalesPersonState = {
  salespersons: [],
};

const salespersonSlice = createSlice({
  name: 'salespersons',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchSalesperson.fulfilled, (state, action) => {
      state.salespersons = action.payload;
    });
  },
});

export default salespersonSlice.reducer;
