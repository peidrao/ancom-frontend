import { createSlice } from '@reduxjs/toolkit';
import { fetchProducts } from '../thunks/productsAction';

interface ProductState {
  products: any[];
}

const initialState: ProductState = {
  products: [],
};

const productslice = createSlice({
  name: 'products',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.products = action.payload;
    });
  },
});

export default productslice.reducer;
