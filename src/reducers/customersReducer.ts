import { createSlice } from '@reduxjs/toolkit';
import { fetchCustomers } from '../thunks/customersAction';

interface CustomerState {
  customers: any[];
}

const initialState: CustomerState = {
  customers: [],
};

const customerSlice = createSlice({
  name: 'customers',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchCustomers.fulfilled, (state, action) => {
      state.customers = action.payload;
    });
  },
});

export default customerSlice.reducer;
